# pylint: disable=trailing-whitespace, redefined-outer-name, missing-docstring
import pytest

from hello_world.app import app


def test_assert():
    assert True


def test_example(client):
    response = client.get("/")
    assert response.status_code == 200
    assert b"hello world!" in response.data


@pytest.fixture
def client():
    test_client = app.test_client()
    return test_client
