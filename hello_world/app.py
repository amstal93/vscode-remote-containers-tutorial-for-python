"""main application module

"""

from flask import Flask

app = Flask(__name__)  # pylint: disable=invalid-name


@app.route("/")
def hello_world():
    """Method that says hello

    Returns:
        String-- Say hello
    """
    return "hello world!"
